package com.sky.mapper;

import com.sky.entity.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;

@Mapper
public interface DishFlavorMapper {
    /**
     * 向口味表中添加多条数据
     * @param flavors
     */
    void insertBatch(List<DishFlavor> flavors);

    /**
     * 批量删除菜品口味
     * @param dishId
     */
    @Delete("delete from  dish_flavor where dish_id=#{dishId}")
    void deleteById(Long dishId);

    /**
     * 根据菜品id查询口味数据
     * @param dishId
     */
    List<DishFlavor>  getByDishId(Long dishId);

}
