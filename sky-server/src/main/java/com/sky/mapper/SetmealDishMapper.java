package com.sky.mapper;

import com.sky.entity.SetmealDish;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealDishMapper {

    /**
     * 根据菜品id查询套餐id
     * @param DishIds
     * @return
     */
    // TODO: 为什么这里传的是list  不是拿单个的dishId去查有没有套餐与他关联吗
    //因为这里是list批量操作  不可能说list中有一个dishId关联上了套餐id   就能操作list整个删除操作
    //select setmeal_id from setmeal_dish where dish_id in (1,2,,3,4)
    List<Long> getSetmealIdsByDishIds(List<Long> DishIds);

    /**
     * 将setmealId放进Setmeal_dish表中
     * @param setmealDishes
     */
    //会有多个添加（list）
    void insertBatch(List<SetmealDish> setmealDishes);

    /**
     * 根据套餐id删除套餐中的菜品
     * @param setmealId
     */
    @Delete("delete from setmeal_dish where setmeal_id =#{setmealId}")
    void deleteBySetmealId(Long setmealId);

    /**
     * 根据套餐id查询菜品
     * @param id
     */
    @Select("select * from setmeal_dish where setmeal_id=#{id}")
    List<SetmealDish> getBySetmealId(Long id);
}
